{ pkgs ? import <nixpkgs> { } }:
with pkgs;

(buildFHSUserEnv {
  name = "acl2s-env";
  targetPkgs = pkgs: (with pkgs; [ openjdk cairo glib gtk3 swt xorg.libXtst ]);
  runScript = "./eclipse";
}).env
