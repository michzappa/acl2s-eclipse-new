; **************** BEGIN INITIALIZATION FOR ACL2s T MODE ****************** ;
; (Nothing to see here!  Your actual file is after this initialization code);

#|

Pete Manolios
Thu Jan 27 18:53:33 EST 2011
----------------------------

The Theorem Proving Beginner level is the next level after the
Beginner level. This is for students who are just starting to
prove theorems.

|#

;Common base theory for all modes.
#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s base theory book.~%") (value :invisible))
(include-book "acl2s/base-theory" :dir :system :ttags :all)


#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading the CCG & prover restrictions book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "prover-restrictions-and-ccg" :uncertified-okp nil :dir :acl2s-modes :ttags ((:ccg) (:prover-restrictions)) :load-compiled-file nil);v4.0 change

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s customizations book.~%") (value :invisible))
(include-book "acl2s/custom" :dir :system :uncertified-okp nil :ttags :all)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem setting up ACL2s Theorem Proving Beginner mode.") (value :invisible))

;Settings common to all ACL2s modes
(acl2s-common-settings)

#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading trace-star and evalable-ld-printing books.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "trace-star" :uncertified-okp nil :dir :acl2s-modes :ttags ((:acl2s-interaction)) :load-compiled-file nil)
(include-book "hacking/evalable-ld-printing" :uncertified-okp nil :dir :system :ttags ((:evalable-ld-printing)) :load-compiled-file nil)

;theory for beginner mode
#+acl2s-startup (er-progn (assign fmt-error-msg "Problem loading ACL2s theorem-proving beginner theory book.~%Please choose \"Recertify ACL2s system books\" under the ACL2s menu and retry after successful recertification.") (value :invisible))
(include-book "theorem-proving-beginner-theory" :dir :acl2s-modes :ttags :all)



#+acl2s-startup (er-progn (assign fmt-error-msg "Problem setting up ACL2s Theorem Proving Beginner mode.") (value :invisible))

;Settings specific to ACL2s Theorem Proving Beginner mode
(acl2s-theorem-proving-beginner-settings)
(acl2::xdoc acl2s::defunc)

(cw "~@0Theorem Proving Beginner mode loaded.~%~@1"
    #+acl2s-startup "${NoMoReSnIp}$~%" #-acl2s-startup ""
    #+acl2s-startup "${SnIpMeHeRe}$~%" #-acl2s-startup "")
    
(acl2::in-package "ACL2S T")

; ***************** END INITIALIZATION FOR ACL2s T MODE ******************* ;
