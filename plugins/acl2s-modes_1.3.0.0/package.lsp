(acl2::in-package "ACL2")

(include-book "data-structures/portcullis" :dir :system)
(include-book "coi/symbol-fns/portcullis" :dir :system)
(include-book "acl2s/portcullis" :dir :system)
